﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Regression
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
    static class DataFromForm {
        public static string tb_PirsonThersholdBetweenControlFactors { get; set; }
        public static string tb_NumericTresholdCorrelationBetweenControlAndControlledFactors { get; set; }
        public static string tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors { get; set; }
        public static bool checkBoxNumericValue { get; set; }
        public static bool checkBoxStatisticalHypothesis { get; set; }
        public static string tb1 { get; set; }
        public static string tb2 { get; set; }
        public static string tb3 { get; set; }
        public static string tb4 { get; set; }
    }
}
