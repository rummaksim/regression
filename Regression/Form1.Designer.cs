﻿namespace Regression
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenShowDialog = new System.Windows.Forms.Button();
            this.tb_ThresholdCorrelationBetweenControlFactors = new System.Windows.Forms.TextBox();
            this.lb_TresholdCorrelationBetweenControlFactros = new System.Windows.Forms.Label();
            this.btn_StartCalculations = new System.Windows.Forms.Button();
            this.btn_ConfirmEnteredData = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros = new System.Windows.Forms.Label();
            this.checkBoxNumericValue = new System.Windows.Forms.CheckBox();
            this.checkBoxStatisticalHypothesis = new System.Windows.Forms.CheckBox();
            this.lb_NumericValue = new System.Windows.Forms.Label();
            this.lb_StatisticalValue = new System.Windows.Forms.Label();
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors = new System.Windows.Forms.TextBox();
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnOpenShowDialog
            // 
            this.btnOpenShowDialog.Location = new System.Drawing.Point(3, 248);
            this.btnOpenShowDialog.Name = "btnOpenShowDialog";
            this.btnOpenShowDialog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnOpenShowDialog.Size = new System.Drawing.Size(166, 23);
            this.btnOpenShowDialog.TabIndex = 0;
            this.btnOpenShowDialog.Text = "Загрузить данные";
            this.btnOpenShowDialog.UseVisualStyleBackColor = true;
            this.btnOpenShowDialog.Click += new System.EventHandler(this.btnOpenShowDialog_Click);
            // 
            // tb_ThresholdCorrelationBetweenControlFactors
            // 
            this.tb_ThresholdCorrelationBetweenControlFactors.Location = new System.Drawing.Point(287, 24);
            this.tb_ThresholdCorrelationBetweenControlFactors.MaxLength = 6;
            this.tb_ThresholdCorrelationBetweenControlFactors.Name = "tb_ThresholdCorrelationBetweenControlFactors";
            this.tb_ThresholdCorrelationBetweenControlFactors.Size = new System.Drawing.Size(101, 20);
            this.tb_ThresholdCorrelationBetweenControlFactors.TabIndex = 1;
            this.tb_ThresholdCorrelationBetweenControlFactors.Text = "0,";
            this.tb_ThresholdCorrelationBetweenControlFactors.TextChanged += new System.EventHandler(this.tb_ThresholdCorrelationBetweenControlFactors_TextChanged);
            this.tb_ThresholdCorrelationBetweenControlFactors.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ThresholdCorrelationBetweenControlFactors_KeyPress);
            // 
            // lb_TresholdCorrelationBetweenControlFactros
            // 
            this.lb_TresholdCorrelationBetweenControlFactros.AutoSize = true;
            this.lb_TresholdCorrelationBetweenControlFactros.Location = new System.Drawing.Point(0, 27);
            this.lb_TresholdCorrelationBetweenControlFactros.Name = "lb_TresholdCorrelationBetweenControlFactros";
            this.lb_TresholdCorrelationBetweenControlFactros.Size = new System.Drawing.Size(281, 13);
            this.lb_TresholdCorrelationBetweenControlFactros.TabIndex = 2;
            this.lb_TresholdCorrelationBetweenControlFactros.Text = "Порог корреляции между управляющими факторами:";
            // 
            // btn_StartCalculations
            // 
            this.btn_StartCalculations.Enabled = false;
            this.btn_StartCalculations.Location = new System.Drawing.Point(347, 248);
            this.btn_StartCalculations.Name = "btn_StartCalculations";
            this.btn_StartCalculations.Size = new System.Drawing.Size(166, 23);
            this.btn_StartCalculations.TabIndex = 3;
            this.btn_StartCalculations.Text = "Начать рассчеты";
            this.btn_StartCalculations.UseVisualStyleBackColor = true;
            this.btn_StartCalculations.Click += new System.EventHandler(this.btn_StartCalculations_Click);
            // 
            // btn_ConfirmEnteredData
            // 
            this.btn_ConfirmEnteredData.Enabled = false;
            this.btn_ConfirmEnteredData.Location = new System.Drawing.Point(175, 248);
            this.btn_ConfirmEnteredData.Name = "btn_ConfirmEnteredData";
            this.btn_ConfirmEnteredData.Size = new System.Drawing.Size(166, 23);
            this.btn_ConfirmEnteredData.TabIndex = 4;
            this.btn_ConfirmEnteredData.Text = "Подтвердить выбор значений";
            this.btn_ConfirmEnteredData.UseVisualStyleBackColor = true;
            this.btn_ConfirmEnteredData.Click += new System.EventHandler(this.btn_ConfirmEnteredData_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.Location = new System.Drawing.Point(519, 248);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(101, 23);
            this.btn_Exit.TabIndex = 5;
            this.btn_Exit.Text = "Выход";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // lb_ConfirmCorrelationBetweenControlAndControlledFactros
            // 
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.AutoSize = true;
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.Location = new System.Drawing.Point(0, 68);
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.Name = "lb_ConfirmCorrelationBetweenControlAndControlledFactros";
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.Size = new System.Drawing.Size(607, 13);
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.TabIndex = 6;
            this.lb_ConfirmCorrelationBetweenControlAndControlledFactros.Text = "Проверка коэффициентов корреляции между управляющими и управляемым фактором произ" +
    "водится на основании:";
            // 
            // checkBoxNumericValue
            // 
            this.checkBoxNumericValue.AutoSize = true;
            this.checkBoxNumericValue.Location = new System.Drawing.Point(3, 84);
            this.checkBoxNumericValue.Name = "checkBoxNumericValue";
            this.checkBoxNumericValue.Size = new System.Drawing.Size(192, 17);
            this.checkBoxNumericValue.TabIndex = 7;
            this.checkBoxNumericValue.Text = "Числового порогового значения";
            this.checkBoxNumericValue.UseVisualStyleBackColor = true;
            this.checkBoxNumericValue.CheckedChanged += new System.EventHandler(this.checkBoxNumericValue_CheckedChanged);
            // 
            // checkBoxStatisticalHypothesis
            // 
            this.checkBoxStatisticalHypothesis.AutoSize = true;
            this.checkBoxStatisticalHypothesis.Location = new System.Drawing.Point(245, 84);
            this.checkBoxStatisticalHypothesis.Name = "checkBoxStatisticalHypothesis";
            this.checkBoxStatisticalHypothesis.Size = new System.Drawing.Size(231, 17);
            this.checkBoxStatisticalHypothesis.TabIndex = 8;
            this.checkBoxStatisticalHypothesis.Text = "На основании статистической гипотезы";
            this.checkBoxStatisticalHypothesis.UseVisualStyleBackColor = true;
            this.checkBoxStatisticalHypothesis.CheckedChanged += new System.EventHandler(this.checkBoxStatisticalHypothesis_CheckedChanged);
            // 
            // lb_NumericValue
            // 
            this.lb_NumericValue.AutoSize = true;
            this.lb_NumericValue.Location = new System.Drawing.Point(0, 104);
            this.lb_NumericValue.Name = "lb_NumericValue";
            this.lb_NumericValue.Size = new System.Drawing.Size(110, 13);
            this.lb_NumericValue.TabIndex = 9;
            this.lb_NumericValue.Text = "Числовое значение:";
            // 
            // lb_StatisticalValue
            // 
            this.lb_StatisticalValue.AutoSize = true;
            this.lb_StatisticalValue.Location = new System.Drawing.Point(242, 104);
            this.lb_StatisticalValue.Name = "lb_StatisticalValue";
            this.lb_StatisticalValue.Size = new System.Drawing.Size(159, 13);
            this.lb_StatisticalValue.TabIndex = 10;
            this.lb_StatisticalValue.Text = "Значение уровня значимости:";
            // 
            // tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors
            // 
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Location = new System.Drawing.Point(407, 101);
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Name = "tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors";
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Size = new System.Drawing.Size(100, 20);
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.TabIndex = 11;
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.TextChanged += new System.EventHandler(this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors_TextChanged);
            this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors_KeyPress);
            // 
            // tb_NumericTresholdCorrelationBetweenControlAndControlledFactors
            // 
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Location = new System.Drawing.Point(116, 101);
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Name = "tb_NumericTresholdCorrelationBetweenControlAndControlledFactors";
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Size = new System.Drawing.Size(100, 20);
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.TabIndex = 12;
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.TextChanged += new System.EventHandler(this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors_TextChanged);
            this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors_KeyPress);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(467, 33);
            this.label1.TabIndex = 13;
            this.label1.Text = "Выбор метода определения лучшей модели: 0 - по умолчанию, 1 - максимальная сумма " +
    "важностей экспертных оценок, 2 - максимальная средняя важность экспертных оценок" +
    "";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Экспертные важностные оценки:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(473, 127);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 17;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(180, 154);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(393, 20);
            this.textBox2.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Управление фактором модели №:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Коэффициент изменения фактора:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(199, 177);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 21;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(199, 203);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 22;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 296);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors);
            this.Controls.Add(this.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors);
            this.Controls.Add(this.lb_StatisticalValue);
            this.Controls.Add(this.lb_NumericValue);
            this.Controls.Add(this.checkBoxStatisticalHypothesis);
            this.Controls.Add(this.checkBoxNumericValue);
            this.Controls.Add(this.lb_ConfirmCorrelationBetweenControlAndControlledFactros);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_ConfirmEnteredData);
            this.Controls.Add(this.btn_StartCalculations);
            this.Controls.Add(this.lb_TresholdCorrelationBetweenControlFactros);
            this.Controls.Add(this.tb_ThresholdCorrelationBetweenControlFactors);
            this.Controls.Add(this.btnOpenShowDialog);
            this.Name = "MainForm";
            this.Text = " ";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnOpenShowDialog;
        public System.Windows.Forms.TextBox tb_ThresholdCorrelationBetweenControlFactors;
        private System.Windows.Forms.Label lb_TresholdCorrelationBetweenControlFactros;
        public System.Windows.Forms.Button btn_StartCalculations;
        private System.Windows.Forms.Button btn_Exit;
        public System.Windows.Forms.Button btn_ConfirmEnteredData;
        private System.Windows.Forms.Label lb_ConfirmCorrelationBetweenControlAndControlledFactros;
        private System.Windows.Forms.CheckBox checkBoxNumericValue;
        private System.Windows.Forms.CheckBox checkBoxStatisticalHypothesis;
        public System.Windows.Forms.Label lb_NumericValue;
        public System.Windows.Forms.Label lb_StatisticalValue;
        public System.Windows.Forms.TextBox tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors;
        public System.Windows.Forms.TextBox tb_NumericTresholdCorrelationBetweenControlAndControlledFactors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
    }
}

