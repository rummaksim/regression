﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using System.IO;

namespace Regression {
    public static class HelpMethods {
        public static StreamWriter outputWithoutFunc = new StreamWriter("outputWithoutFunc.txt");
        public static StreamWriter outputWithFunc = new StreamWriter("outputWithFunc.txt");
        /// <summary>
        /// Возвращает имя колонки в Excel с данным номером
        /// </summary>
        /// <param name="columnCount">Номер колонки</param>
        /// <returns>Возвращает имя колонки в Excel с данным номером</returns>
        public static string GetColumnName(int columnCount) {
            if (columnCount < 27)
                return ((char)(65 + columnCount - 1)).ToString();
            return ((char)(65 + columnCount / 26 - 1)).ToString() + ((char)(65 + columnCount % 26 - 1)).ToString();
        }
        /// <summary>
        /// Возвращает массив со значениями корреляций между всеми управляющими факторами
        /// </summary>
        /// <param name="controlFactorsValues">Массив со значениями управляющих факторов</param>
        /// <returns>Массив со значениями корреляций между всеми управляющими факторами</returns>
        public static List<List<double>> GetCorrelationBetweenControlFactors(List<List<double>> controlFactorsValues) {
            var correlationBetweenControlFactros = new List<List<double>>();
            var controlFactorsAverageValues = new List<double>();
            var controlFactorsCount = controlFactorsValues[0].Count(); //кол-во  управляющих факторов, для любого controlFactorsValues[i] оно одинаково, беру 0.
            var controlFactorsCountValues = controlFactorsValues.Count(); //кол-во значений каждого управляющего фактора
            for (int i = 0; i < controlFactorsCount; i++) {
                controlFactorsAverageValues.Add(0);
                for (int j = 0; j < controlFactorsCountValues; j++)
                    controlFactorsAverageValues[i] += controlFactorsValues[j][i];
                controlFactorsAverageValues[i] /= controlFactorsCountValues; //посчитал среднее значение каждого упр. фактора
            }
            double numerator = 0;  //числитель корреляционной дроби
            double denumeratorFirstPart = 0; //первая сумма знаменателя корреляционной дроби
            double denumeratorSecondPart = 0; // вторая сумма знаменателя корреляционной дроби
            for (int i = 0; i < controlFactorsCount; i++) {
                correlationBetweenControlFactros.Add(new List<double>());
                for (int j = 0; j < controlFactorsCount; j++)
                    correlationBetweenControlFactros[i].Add(0);
            }
            for (int i = 0; i < controlFactorsCount; i++) //матрица корреляций симметрична, вычисляю только верхний треугольник
                for (int j = i; j < controlFactorsCount; j++) { //заполнение матрицы корреляций
                    for (int l = 0; l < controlFactorsCountValues; l++) {
                        numerator += (controlFactorsValues[l][j] - controlFactorsAverageValues[j]) * (controlFactorsValues[l][i] - controlFactorsAverageValues[i]);
                        denumeratorFirstPart += Math.Pow((controlFactorsValues[l][j] - controlFactorsAverageValues[j]), 2);
                        denumeratorSecondPart += Math.Pow((controlFactorsValues[l][i] - controlFactorsAverageValues[i]), 2);
                    }
                    correlationBetweenControlFactros[i][j] = numerator / (Math.Pow(denumeratorFirstPart * denumeratorSecondPart, 0.5));
                    numerator = denumeratorFirstPart = denumeratorSecondPart = 0;
                }
            for (int i = 1; i < controlFactorsCount; i++) //дополняю нижний треугольник матрицы
                for (int j = 0; j < i; j++)
                    correlationBetweenControlFactros[i][j] = correlationBetweenControlFactros[j][i];
            return correlationBetweenControlFactros;
        }
        /// <summary>
        /// По массиву корреляций между управляющими факторами и порогу определяет, какие управляющие факторы могут быть в одном уравнении
        /// </summary>
        /// <param name="correlationBetweenControlFactors">Массив корреляций между управляющими факторами</param>
        /// <param name="thershold">Порог корреляции</param>
        /// <returns>Массив, содержащий информацию какие управляющие факторы могут быть в одном уравнении</returns>
        public static List<List<double>> GetDependenceBetweenControlFactorsByPirsonCorrelationCoefficient(List<List<double>> correlationBetweenControlFactors, double thershold) {
            var dependenceBetweenControlFactorsByPirsonCorrelationCoefficient = new List<List<double>>();
            var controlFactorsCount = correlationBetweenControlFactors.Count();
            for (int i = 0; i < controlFactorsCount; i++) {
                dependenceBetweenControlFactorsByPirsonCorrelationCoefficient.Add(new List<double>());
                for (int j = 0; j < controlFactorsCount; j++)
                    dependenceBetweenControlFactorsByPirsonCorrelationCoefficient[i].Add(0);
            }
            for (int i = 0; i < controlFactorsCount; i++)
                for (int j = 0; j < controlFactorsCount; j++)
                    if (Math.Abs(correlationBetweenControlFactors[i][j]) < thershold)
                        dependenceBetweenControlFactorsByPirsonCorrelationCoefficient[i][j] = 1;
                    else
                        dependenceBetweenControlFactorsByPirsonCorrelationCoefficient[i][j] = 0;
            return dependenceBetweenControlFactorsByPirsonCorrelationCoefficient;
        }
        /// <summary>
        /// Возвращает массив корреляций между управляемым и управляющими факторами
        /// </summary>
        /// <param name="controlFactorsValues"> Значения упраляющих факторов </param>
        /// <param name="controlledFactorsValues"> Значения управляемого фактора </param>
        /// <returns>Массив корреляций между управляемым и управляющими факторами</returns>
        public static List<double> GetCorrelationBetweenControlAndControlledFactors(List<List<double>> controlFactorsValues,
            List<double> controlledFactorsValues) {
            var correlationBetweenControlAndControlledFactors = new List<double>();
            var controlFactorsAverageValues = new List<double>();
            var controlFactorsCount = controlFactorsValues[0].Count(); //кол-во  управляющих факторов, для любого controlFactorsValues[i] оно одинаково, беру 0.
            var controlFactorsCountValues = controlFactorsValues.Count(); //кол-во значений каждого управляющего фактора
            double controlledFactorsAverageValue = 0; //среднее значение управляемого фактора
            for (int i = 0; i < controlFactorsCountValues; i++)
                controlledFactorsAverageValue += controlledFactorsValues[i];
            controlledFactorsAverageValue /= controlFactorsCountValues;
            for (int i = 0; i < controlFactorsCount; i++) {
                controlFactorsAverageValues.Add(0);
                for (int j = 0; j < controlFactorsCountValues; j++)
                    controlFactorsAverageValues[i] += controlFactorsValues[j][i];
                controlFactorsAverageValues[i] /= controlFactorsCountValues; //посчитал среднее значение каждого упр. фактора
            }
            double numerator = 0;  //числитель корреляционной дроби
            double denumeratorFirstPart = 0; //первая сумма знаменателя корреляционной дроби
            double denumeratorSecondPart = 0; // вторая сумма знаменателя корреляционной дроби
            for (int i = 0; i < controlFactorsCount; i++) { //матрица корреляций симметрична, вычисляю только верхний треугольник
                for (int j = 0; j < controlFactorsCountValues; j++) { //заполнение матрицы корреляций
                    numerator += (controlFactorsValues[j][i] - controlFactorsAverageValues[i]) * (controlledFactorsValues[j] - controlledFactorsAverageValue);
                    denumeratorFirstPart += Math.Pow((controlFactorsValues[j][i] - controlFactorsAverageValues[i]), 2);
                    denumeratorSecondPart += Math.Pow((controlledFactorsValues[j] - controlledFactorsAverageValue), 2);
                }
                correlationBetweenControlAndControlledFactors.Add(numerator / (Math.Pow(denumeratorFirstPart * denumeratorSecondPart, 0.5)));
                numerator = denumeratorFirstPart = denumeratorSecondPart = 0;
            }
            return correlationBetweenControlAndControlledFactors;
        }

        public static double GetCorrelationBetweenControlAndControlledFactors(List<double> controlFactorsValues,
          List<double> controlledFactorsValues) {
            double controlFactorsAverageValue = 0;
            double controlledFactorsAverageValue = 0;
            var controlFactorsCount = controlFactorsValues.Count;
            for (int i = 0; i < controlFactorsCount; i++)
                controlledFactorsAverageValue += controlledFactorsValues[i];
            controlledFactorsAverageValue /= controlFactorsCount;
            for (int i = 0; i < controlFactorsCount; i++) {
                controlFactorsAverageValue += controlFactorsValues[i];
            }
            controlFactorsAverageValue /= controlFactorsCount;
            double numerator = 0;  //числитель корреляционной дроби
            double denumeratorFirstPart = 0; //первая сумма знаменателя корреляционной дроби
            double denumeratorSecondPart = 0; // вторая сумма знаменателя корреляционной дроби
            for (int i = 0; i < controlFactorsCount; i++) { //матрица корреляций симметрична, вычисляю только верхний треугольник

                numerator += (controlFactorsValues[i] - controlFactorsAverageValue) * (controlledFactorsValues[i] - controlledFactorsAverageValue);
                denumeratorFirstPart += Math.Pow((controlFactorsValues[i] - controlFactorsAverageValue), 2);
                denumeratorSecondPart += Math.Pow((controlledFactorsValues[i] - controlledFactorsAverageValue), 2);
            }
            return (numerator / (Math.Pow(denumeratorFirstPart * denumeratorSecondPart, 0.5)));
        }

        /// <summary>
        /// По массиву корреляций между управляющими и управляемым фактором определяет, какие управдяющие факторы могут быть в уравнении
        /// </summary>
        /// <param name="correlationBetweenControlAndControlledFactors"> Массив корреляций между управляющими и управляемым фактором</param>
        /// <param name="thershold">Числовой порог корреляции</param>
        /// <returns>Управляющие факторы, которые можно включить в уравнение</returns>
        public static List<double> GetNumericDependenceBetweenControlAndControlledFactors(List<double> correlationBetweenControlAndControlledFactors, double thershold) {
            var controlFactorsCount = correlationBetweenControlAndControlledFactors.Count;
            var dependenceBetweenControlAndControlledFactors = new List<double>();
            for (int i = 0; i < controlFactorsCount; i++)
                if (Math.Abs(correlationBetweenControlAndControlledFactors[i]) < thershold)
                    dependenceBetweenControlAndControlledFactors.Add(0);
                else
                    dependenceBetweenControlAndControlledFactors.Add(1);
            return dependenceBetweenControlAndControlledFactors;
        }
        /// <summary>
        /// Получает критическое значение критерия Сьюдента для заданного числа элементов выборки и уровня значимости
        /// </summary>
        /// <param name="countControlledFactorValues">Количество элементов выборки</param>
        /// <param name="alpha">Уровень значимости</param>
        /// <returns>Критическое значение критерия Стьюдента</returns>
        public static double GetCriticalStudentValue(int countControlledFactorValues, double alpha) {
            double v = 0.5, dv = 0.5, criticalValue = 0;
            while (dv > 1e-10) {
                criticalValue = 1.0 / v - 1.0;
                dv /= 2;
                if (GetCriticalStudentValueHelpMethodT(criticalValue, countControlledFactorValues) > alpha)
                    v -= dv;
                else
                    v += dv;
            }
            return criticalValue;
        }
        public static double GetCriticalStudentValueHelpMethodT(double criticalValue, int countControlledFactorValues) {
            double th = Math.Atan(Math.Abs(criticalValue) / Math.Sqrt((double)countControlledFactorValues));
            double pi2 = Math.Acos(-1.0) / 2;
            if (countControlledFactorValues == 1)
                return (1 - th / pi2);
            double sth = Math.Sin(th), cth = Math.Cos(th);
            if (countControlledFactorValues % 2 == 1)
                return (1 - (th + sth * cth * GetCriticalStudentValueHelpMethodStatCom(cth * cth, 2, countControlledFactorValues - 3, -1)) / pi2);
            return (1 - sth * GetCriticalStudentValueHelpMethodStatCom(cth * cth, 1, countControlledFactorValues - 3, -1));
        }
        public static double GetCriticalStudentValueHelpMethodStatCom(double q, int i, int j, int b) {
            double zz = 1, z = zz;
            int k = i;
            while (k <= j) {
                zz *= q * k / (k - b);
                z += zz;
                k += 2;
            }
            return z;
        }
        /// <summary>
        /// На основании критерия Стьюдента получает информацию, какие управляющие факторы влияют на управляемый фактор
        /// </summary>
        /// <param name="correlationBetweenControlAndControlledFactors">Корреляция между управляющими и управляемым фактором</param>
        /// <param name="alpha">Уровень значимости</param>
        /// <param name="countControlledFactorValues">Количество значений управляющих/управляемого фактора</param>
        /// <returns></returns>
        public static List<double> GetStatisticalDependenceBetweenControlAndControlledFactors(List<double> correlationBetweenControlAndControlledFactors, double alpha, int countControlledFactorValues) {
            double studentCriticalValue = GetCriticalStudentValue(countControlledFactorValues, alpha);
            double studentCurrentElementValue = 0;
            var dependenceBetweenControlAndControlledFactors = new List<double>();
            for (int i = 0; i < correlationBetweenControlAndControlledFactors.Count; i++) {
                studentCurrentElementValue = Math.Abs(correlationBetweenControlAndControlledFactors[i] * Math.Sqrt((countControlledFactorValues - 2) / (1 - Math.Pow(correlationBetweenControlAndControlledFactors[i], 2))));
                if (studentCurrentElementValue <= studentCriticalValue)
                    dependenceBetweenControlAndControlledFactors.Add(0);
                else
                    dependenceBetweenControlAndControlledFactors.Add(1);
            }
            return dependenceBetweenControlAndControlledFactors;
        }
        /// <summary>
        /// Возвращает управвляющие факторы, которые могут входить в регрессионное уравнение и информацию о том, какие из них не смогут быть в одном уравнении одновременно
        /// </summary>
        /// <param name="dependenceBetweenControlFactors">Зависимость между управляющими факторами</param>
        /// <param name="dependenceBetweenControlAndControlledFactors">Зависимость между управляющими и управляемым фактором</param>
        /// <returns>Управвляющие факторы, которые могут входить в регрессионное уравнение и информацию о том, какие из них не смогут быть в одном уравнении одновременно</returns>
        public static List<List<double>> GetFinalDependenceBetweenControlFactors(List<List<double>> dependenceBetweenControlFactors, List<double> dependenceBetweenControlAndControlledFactors) {
            var finalDependenceBetweenControlFactors = new List<List<double>>();
            var countControlFactors = dependenceBetweenControlFactors.Count;
            for (int i = 0; i < countControlFactors; i++) {
                finalDependenceBetweenControlFactors.Add(new List<double>());
                for (int j = 0; j < countControlFactors; j++)
                    finalDependenceBetweenControlFactors[i].Add(0);
            }
            for (int i = 0; i < countControlFactors; i++)
                if (dependenceBetweenControlAndControlledFactors[i] != 0)
                    for (int j = i + 1; j < countControlFactors; j++) {
                        if (j == i + 1)
                            finalDependenceBetweenControlFactors[i][j - 1] = 1;
                        if (dependenceBetweenControlAndControlledFactors[j] != 0)
                            finalDependenceBetweenControlFactors[i][j] = dependenceBetweenControlFactors[i][j];
                    }
            for (int i = 1; i < countControlFactors; i++) //дополняю нижний треугольник матрицы
                for (int j = 0; j < i; j++)
                    finalDependenceBetweenControlFactors[i][j] = finalDependenceBetweenControlFactors[j][i];
            return finalDependenceBetweenControlFactors;
        }
        public static List<List<double>> CreateRegressionEquationsModels(List<List<double>> dependenceBetweenControlFactors) {
            var regressionEquationsModels = new List<List<double>>();
            var dependenceInCurrentRow = new List<double>(); //текущая строка массива зависимостей
            var countControlFactors = dependenceBetweenControlFactors[0].Count;
            int i = 0;
            while (i < countControlFactors) {
                dependenceInCurrentRow = dependenceBetweenControlFactors[i];
                while (true) {
                    if (dependenceBetweenControlFactors[i][i] == 0)
                        break;
                    for (int j = i; j < countControlFactors; j++) {
                        if ((i == j) && (dependenceBetweenControlFactors[i][j] == 1)) {
                            regressionEquationsModels.Add(new List<double>());
                            regressionEquationsModels[regressionEquationsModels.Count - 1].Add(j);
                        }
                        else {
                            bool intoModel = true;
                            if (dependenceBetweenControlFactors[i][j] == 1) {
                                foreach (var e in regressionEquationsModels[regressionEquationsModels.Count - 1]) {
                                    if (dependenceBetweenControlFactors[(int)e][j] != 1) {
                                        intoModel = false;
                                        break;
                                    }
                                }
                                if (intoModel)
                                    regressionEquationsModels[regressionEquationsModels.Count - 1].Add(j);
                            }
                        }
                    }
                    if (regressionEquationsModels.Count == 0 || regressionEquationsModels[regressionEquationsModels.Count - 1].Count < 2) {
                        int _currentModelsCount = regressionEquationsModels.Count - 1;
                        for (int p = 0; p < regressionEquationsModels.Count - 1; p++)//отброс лишних моделей
                            for (int q = 0; q < regressionEquationsModels[regressionEquationsModels.Count - 1].Count; q++)
                                if (!regressionEquationsModels[p].Contains(regressionEquationsModels[regressionEquationsModels.Count - 1][q])) {
                                    _currentModelsCount--;
                                    break;
                                }
                        if (_currentModelsCount != 0)
                            regressionEquationsModels.Remove(regressionEquationsModels[regressionEquationsModels.Count - 1]);
                        break;
                    }

                    var index = regressionEquationsModels[regressionEquationsModels.Count - 1].Count - 1;
                    var value = regressionEquationsModels[regressionEquationsModels.Count - 1][index];
                    dependenceBetweenControlFactors[i][(int)value] = 0;
                    for (int k = (int)value + 1; k < countControlFactors; k++)
                        dependenceBetweenControlFactors[i][k] = dependenceInCurrentRow[k];

                    int currentModelsCount = regressionEquationsModels.Count - 1;
                    for (int p = 0; p < regressionEquationsModels.Count - 1; p++)//отброс лишних моделей
                        for (int q = 0; q < regressionEquationsModels[regressionEquationsModels.Count - 1].Count; q++)
                            if (!regressionEquationsModels[p].Contains(regressionEquationsModels[regressionEquationsModels.Count - 1][q])) {
                                currentModelsCount--;
                                break;
                            }
                    if (currentModelsCount != 0)
                        regressionEquationsModels.Remove(regressionEquationsModels[regressionEquationsModels.Count - 1]);
                }
                for (int k = 0; k < countControlFactors; k++)
                    dependenceBetweenControlFactors[i][k] = dependenceInCurrentRow[k];
                i++;
            }
            return regressionEquationsModels;
        }
        /// <summary>
        /// Получение вектора коэффициентов без функциональной предобработки
        /// </summary>
        /// <param name="controlFactorsValues"></param>
        /// <param name="controlledFactorsValues"></param>
        /// <param name="model"></param>
        public static void CreateEquationsWithoutFunc(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model) {
            var X = new List<List<double>>();
            var B = new List<double>();
            var e = new List<double>();
            var Y = new List<double>();
            foreach (var elem in controlledFactorsValues)
                Y.Add(elem);
            for (int q = 0; q < controlFactorsValues.Count; q++) {
                X.Add(new List<double>());
                X[q].Add(1);
                for (int p = 0; p < model.Count; p++) {
                    X[q].Add(controlFactorsValues[q][(int)model[p]]);
                }
            }
            var XT = MatrixTranspon(X);
            var XTX = MatrixPermutation(XT, X);
            var XTY = MatrixPermutation(XT, Y);
            var XTX_1 = new List<List<double>>();
            for (int i = 0; i < XTX.Count; i++) {
                XTX_1.Add(new List<double>());
                for (int j = 0; j < XTX.Count; j++)
                    XTX_1[i].Add(0);
            }
            var determinant = GetMatrixDeterminant(XTX);
            for (int i = 0; i < XTX.Count; i++) {
                for (int j = 0; j < XTX.Count; j++) {
                    int m = XTX.Count - 1;
                    var tempMatrix = InverseMatrixHelpFunction(XTX, i, j);
                    XTX_1[i][j] = Math.Pow(-1.0, i + j + 2) * GetMatrixDeterminant(tempMatrix) / determinant; //Получена XTX^-1
                }
            }
            for (int i = 0; i < XTX.Count; i++) {
                B.Add(0);
                for (int t = 0; t < XTX.Count; t++)
                    B[i] += XTX_1[i][t] * XTY[t]; // получен вектор B
            }
            outputDataWithoutFunc(controlFactorsValues, controlledFactorsValues, model, B);
        }

        public static List <double> CreateEquationsWithFunc(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model) {
            var X = new List<List<double>>();
            var B = new List<double>();
            var e = new List<double>();
            var Y = new List<double>();
            foreach (var elem in controlledFactorsValues)
                Y.Add(elem);
            for (int q = 0; q < controlFactorsValues.Count; q++) {
                X.Add(new List<double>());
                X[q].Add(1);
                for (int p = 0; p < model.Count; p++) {
                    X[q].Add(controlFactorsValues[q][(int)model[p]]);
                }
            }
            var XT = MatrixTranspon(X);
            var XTX = MatrixPermutation(XT, X);
            var XTY = MatrixPermutation(XT, Y);
            var XTX_1 = new List<List<double>>();
            for (int i = 0; i < XTX.Count; i++) {
                XTX_1.Add(new List<double>());
                for (int j = 0; j < XTX.Count; j++)
                    XTX_1[i].Add(0);
            }
            var determinant = GetMatrixDeterminant(XTX);
            for (int i = 0; i < XTX.Count; i++) {
                for (int j = 0; j < XTX.Count; j++) {
                    int m = XTX.Count - 1;
                    var tempMatrix = InverseMatrixHelpFunction(XTX, i, j);
                    XTX_1[i][j] = Math.Pow(-1.0, i + j + 2) * GetMatrixDeterminant(tempMatrix) / determinant; //Получена XTX^-1
                }
            }
            for (int i = 0; i < XTX.Count; i++) {
                B.Add(0);
                for (int t = 0; t < XTX.Count; t++)
                    B[i] += XTX_1[i][t] * XTY[t]; // получен вектор B
            }
            outputDataWithFunc(controlFactorsValues, controlledFactorsValues, model, B);
            return B;
        }

        public static List<double> CreateEquationsForControlFactors(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model) {
            var X = new List<List<double>>();
            var B = new List<double>();
            var e = new List<double>();
            var Y = new List<double>();
            foreach (var elem in controlledFactorsValues)
                Y.Add(elem);
            for (int q = 0; q < controlFactorsValues.Count; q++) {
                X.Add(new List<double>());
                X[q].Add(1);
                for (int p = 0; p < model.Count; p++) {
                    X[q].Add(controlFactorsValues[q][(int)model[p]]);
                }
            }
            var XT = MatrixTranspon(X);
            var XTX = MatrixPermutation(XT, X);
            var XTY = MatrixPermutation(XT, Y);
            var XTX_1 = new List<List<double>>();
            for (int i = 0; i < XTX.Count; i++) {
                XTX_1.Add(new List<double>());
                for (int j = 0; j < XTX.Count; j++)
                    XTX_1[i].Add(0);
            }
            var determinant = GetMatrixDeterminant(XTX);
            for (int i = 0; i < XTX.Count; i++) {
                for (int j = 0; j < XTX.Count; j++) {
                    int m = XTX.Count - 1;
                    var tempMatrix = InverseMatrixHelpFunction(XTX, i, j);
                    XTX_1[i][j] = Math.Pow(-1.0, i + j + 2) * GetMatrixDeterminant(tempMatrix) / determinant; //Получена XTX^-1
                }
            }
            for (int i = 0; i < XTX.Count; i++) {
                B.Add(0);
                for (int t = 0; t < XTX.Count; t++)
                    B[i] += XTX_1[i][t] * XTY[t]; // получен вектор B
            }
            return B;
        }

        /// <summary>
        /// Вывод в  текстовик
        /// </summary>
        /// <param name="controlFactorsValues"></param>
        /// <param name="controlledFactorsValues"></param>
        /// <param name="model"></param>
        /// <param name="B"></param>
        public static void outputDataWithoutFunc(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model, List<double> B) {
            outputWithoutFunc.WriteLine("Регрессионное уравнение:");
            outputWithoutFunc.Write(B[0]);
            for (int i = 0; i < model.Count; i++)
                outputWithoutFunc.Write(" + ({0})X{1}", B[i + 1], model[i] + 1);
            outputWithoutFunc.WriteLine();
            outputWithoutFunc.WriteLine("Коэффициенты модели значимы на уровне значимости alpha=0.05");
            outputWithoutFunc.WriteLine("Коэффициент детерминации = {0}", GetDeterminationCoefficient(controlFactorsValues, controlledFactorsValues, model, B));
            outputWithoutFunc.WriteLine("-------------------------------------------------------------------------------");
           // outputWithoutFunc.Close();
        }

        public static void outputDataWithFunc(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model, List<double> B) {
            outputWithFunc.WriteLine("Регрессионное уравнение:");
            outputWithFunc.Write(B[0]);
            for (int i = 0; i < model.Count; i++)
                outputWithFunc.Write(" + ({0})X{1}", B[i + 1], model[i] + 1);
            outputWithFunc.WriteLine();
            outputWithFunc.WriteLine("Коэффициенты модели значимы на уровне значимости alpha=0.05");
            outputWithFunc.WriteLine("Коэффициент детерминации = {0}", GetDeterminationCoefficient(controlFactorsValues, controlledFactorsValues, model, B));
            outputWithFunc.WriteLine("-------------------------------------------------------------------------------");
           // outputWithFunc.Close();
        }

        /// <summary>
        /// Вычисление коэффициента детерминации
        /// </summary>
        /// <param name="controlFactorsValues"></param>
        /// <param name="controlledFactorsValue"></param>
        /// <param name="model"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static double GetDeterminationCoefficient(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues, List<double> model, List<double> B) {
            double yAverageObservable = 0;
            var yPredictive = new List<double>();
            for (int i = 0; i < controlledFactorsValues.Count; i++)
                yAverageObservable += controlledFactorsValues[i];
            yAverageObservable /= controlledFactorsValues.Count;
            double sum;
            for (int i = 0; i < controlledFactorsValues.Count; i++) {
                sum = B[0];
                for (int j = 0; j < model.Count; j++)
                    sum += controlFactorsValues[i][(int)model[j]] * B[j + 1];
                yPredictive.Add(sum);
            }
            double numerator = 0, denumerator = 0;
            for (int i = 0; i < controlledFactorsValues.Count; i++)
                numerator += Math.Pow(controlledFactorsValues[i] - yPredictive[i], 2);
            for (int i = 0; i < controlledFactorsValues.Count; i++)
                denumerator += Math.Pow(controlledFactorsValues[i] - yAverageObservable, 2);
            return 1 - numerator / denumerator;
        }

        /// <summary>
        /// Транспонирование матрицы
        /// </summary>
        /// <param name="matrix"> Исходная матрица</param>
        /// <returns> Транспонированная матрица</returns>
        public static List<List<double>> MatrixTranspon(List<List<double>> matrix) {
            var result = new List<List<double>>();
            var rowsCount = matrix.Count;
            var columnsCount = matrix[0].Count;
            for (int i = 0; i < columnsCount; i++) {
                result.Add(new List<double>());
                for (int j = 0; j < rowsCount; j++)
                    result[i].Add(matrix[j][i]);
            }
            return result;

        }
        /// <summary>
        /// Перемножение матриц
        /// </summary>
        /// <param name="firstMatrix">Первая матрица</param>
        /// <param name="secondMatrix">Вторая матрица</param>
        /// <returns>Результирующая матрица</returns>
        public static List<List<double>> MatrixPermutation(List<List<double>> firstMatrix, List<List<double>> secondMatrix) {
            var resultMatrix = new List<List<double>>();
            double sum;
            for (int i = 0; i < firstMatrix.Count; i++) {
                resultMatrix.Add(new List<double>());
                for (int j = 0; j < secondMatrix[0].Count; j++) {
                    sum = 0;
                    for (int k = 0; k < firstMatrix[0].Count; k++)
                        sum += firstMatrix[i][k] * secondMatrix[k][j];
                    resultMatrix[i].Add(sum);
                }
            }
            return resultMatrix;
        }
        /// <summary>
        /// Перемножение матрицы и вектора
        /// </summary>
        /// <param name="firstMatrix">Матрица</param>
        /// <param name="secondMatrix">Вектор</param>
        /// <returns>Результирующая матрица</returns>
        public static List<double> MatrixPermutation(List<List<double>> firstMatrix, List<double> secondMatrix) {
            var resultMatrix = new List<double>();
            double sum;
            for (int i = 0; i < firstMatrix.Count; i++) {
                sum = 0;
                for (int k = 0; k < firstMatrix[0].Count; k++)
                    sum += firstMatrix[i][k] * secondMatrix[k];
                resultMatrix.Add(sum);
            }
            return resultMatrix;
        }
        /// <summary>
        /// Возвращает определитель квадратной матрицы
        /// </summary>
        /// <param name="matrix"> Квадратная матрица </param>
        /// <returns>Определитель матрицы</returns>
        public static double GetMatrixDeterminant(List<List<double>> matrix) {
            var saveMatrix = new List<List<double>>();
            for (int i = 0; i < matrix.Count; i++) {
                saveMatrix.Add(new List<double>());
                for (int j = 0; j < matrix.Count; j++)
                    saveMatrix[i].Add(matrix[i][j]);
            }
            double det = 1;
            double b = 0;
            for (int i = 0; i < matrix.Count; i++) {
                for (int j = i + 1; j < matrix.Count; j++) {
                    if (saveMatrix[i][i] == 0)
                        if (saveMatrix[i][j] == 0)
                            b = 0;
                        else
                            return 0;
                    else b = saveMatrix[j][i] / saveMatrix[i][i];
                    for (int k = i; k < matrix.Count; k++)
                        saveMatrix[j][k] = saveMatrix[j][k] - saveMatrix[i][k] * b;
                }
                det *= saveMatrix[i][i];
            }
            return det;
        }
        public static List<List<double>> InverseMatrixHelpFunction(List<List<double>> matrix, int indRow, int indCol) {
            int ki = 0;
            var tempMatrix = new List<List<double>>();
            for (int i = 0; i < matrix.Count - 1; i++) {
                tempMatrix.Add(new List<double>());
            }
            for (int i = 0; i < matrix.Count; i++) {
                if (i != indRow) {
                    for (int j = 0; j < matrix.Count; j++)
                        if (j != indCol)
                            tempMatrix[ki].Add(matrix[i][j]);
                    ki++;
                }
            }
            return tempMatrix;
        }
        /// <summary>
        /// Переводит управляющие факторы на интервал 2-3
        /// </summary>
        /// <param name="controlFactorsValues"></param>
        /// <returns></returns>
        public static List<List<double>> TransferControlFactorsToInterval2_3(List<List<double>> controlFactorsValues) {
            var transferedValues = new List<List<double>>();
            for (int i = 0; i < controlFactorsValues.Count; i++)
                transferedValues.Add(new List<double>());
            var maxColumnValues = new List<double>();
            var minColumnValues = new List<double>();
            for (int i = 0; i < controlFactorsValues[0].Count; i++) {
                double max = double.MinValue;
                double min = double.MaxValue;
                for (int j = 0; j < controlFactorsValues.Count; j++) {
                    if (controlFactorsValues[j][i] < min)
                        min = controlFactorsValues[j][i];
                    if (controlFactorsValues[j][i] > max)
                        max = controlFactorsValues[j][i];
                }
                maxColumnValues.Add(max);
                minColumnValues.Add(min);
            }
            for (int i = 0; i < controlFactorsValues.Count; i++)
                for (int j = 0; j < controlFactorsValues[0].Count; j++)
                    transferedValues[i].Add(2 + (controlFactorsValues[i][j] - minColumnValues[j]) / (maxColumnValues[j] - minColumnValues[j]));
            return transferedValues;
        }
        /// <summary>
        /// переводит на интервал 2-3
        /// </summary>
        /// <param name="controlFactorsValues"></param>
        /// <returns></returns>
        public static List<double> TransferControlFactorsToInterval2_3(List<double> controlFactorsValues) {
            var transferedValues = new List<double>();
            double max = double.MinValue;
            double min = double.MaxValue;
            for (int i = 0; i < controlFactorsValues.Count; i++) {
                if (controlFactorsValues[i] < min)
                    min = controlFactorsValues[i];
                if (controlFactorsValues[i] > max)
                    max = controlFactorsValues[i];
            }
            for (int i = 0; i < controlFactorsValues.Count; i++)
                transferedValues.Add(2 + (controlFactorsValues[i] - min) / (max - min));
            return transferedValues;
        }






    }
}