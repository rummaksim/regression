﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Regression;
using System.Text;
using System.IO;

namespace Regression {
    public static class RegressionEquations {
        /// <summary>
        /// Отделяет числовые данные из Excel от нечисловых, разделяет числовые факторы на управляющие и управляемые
        /// </summary>
        /// <param name="dataFromExcel">object[,] данные из Excel</param>
        public static void GetDataFromExcel(object[,] dataFromExcel) {
            var controlFactorsValues = new List<List<double>>(); //значения управляющих факторов
            var controlledFactorsValues = new List<double>(); // значения управляемых факторов(по умолчанию только последний столбец)
            var controlFactrosNames = new List<string>(); //имена управляющих факторов
            var controlledFactrosNames = new List<string>(); //именя управляемых факторов
            var startColumn = 3; //номер столбца, с которого начинаются данные. Нумерация с 1.
            var startRow = 2; //номер строки, с которой начинаются данные. Нумерация с 1.
            for (int i = startRow; i <= dataFromExcel.GetLength(0); i++) {
                controlFactorsValues.Add(new List<double>());
                int j;
                for (j = startColumn; j <= dataFromExcel.GetLength(1) - 1; j++)
                    controlFactorsValues[i - startRow].Add(Convert.ToDouble(dataFromExcel[i, j]));
                controlledFactorsValues.Add(Convert.ToDouble(dataFromExcel[i, j]));
            }
            int k;
            for (k = startColumn; k <= dataFromExcel.GetLength(1) - 1; k++)
                controlFactrosNames.Add(dataFromExcel[1, k].ToString());
            controlledFactrosNames.Add(dataFromExcel[1, k].ToString());
            CreateRegressionEquations(controlFactorsValues, controlledFactorsValues, controlFactrosNames, controlledFactrosNames);
        }

        public static bool GetError(List<double> lastIteration, List<double> currentIteration) {
            int count = lastIteration.Count;
            var error = 0.001;
            for (int i = 0; i < count; i++)
                if (currentIteration[i] - lastIteration[i] > error)
                    return false;
            return true;
        }

        public static void CreateRegressionEquations(List<List<double>> controlFactorsValues, List<double> controlledFactorsValues,
            List<string> controlFactrosNames, List<string> ControlledFactrosNames) {
            var correlationBetweenControlFactors = HelpMethods.GetCorrelationBetweenControlFactors(controlFactorsValues); // массив корреляций между упр. факторами
            var dependenceBetweenControlFactors = HelpMethods.GetDependenceBetweenControlFactorsByPirsonCorrelationCoefficient(correlationBetweenControlFactors, double.Parse(DataFromForm.tb_PirsonThersholdBetweenControlFactors));
            var correlationBetweenControlAndControlledFactors = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorsValues, controlledFactorsValues);
            var dependenceBetweenControlAndControlledFactors = new List<double>();
            if (DataFromForm.checkBoxNumericValue)
                dependenceBetweenControlAndControlledFactors = HelpMethods.GetNumericDependenceBetweenControlAndControlledFactors(correlationBetweenControlAndControlledFactors, double.Parse(DataFromForm.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors));
            else
                dependenceBetweenControlAndControlledFactors = HelpMethods.GetStatisticalDependenceBetweenControlAndControlledFactors(correlationBetweenControlAndControlledFactors, double.Parse(DataFromForm.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors), controlledFactorsValues.Count);
            var finalDependenceBetweenControlFactors = HelpMethods.GetFinalDependenceBetweenControlFactors(dependenceBetweenControlFactors, dependenceBetweenControlAndControlledFactors);
            var regressionEquationsModels = HelpMethods.CreateRegressionEquationsModels(finalDependenceBetweenControlFactors); //макеты моделей
            for (int i = 0; i < regressionEquationsModels.Count; i++) { //создание моделей с коэффициентами без предобработки
                HelpMethods.CreateEquationsWithoutFunc(controlFactorsValues, controlledFactorsValues, regressionEquationsModels[i]);
            }
            //создание моделей с функциональной предобработкой
            var transferedControlFactorsValues = HelpMethods.TransferControlFactorsToInterval2_3(controlFactorsValues);
            var controlFactorsValuesTransfer = new List<List<double>>();
            for (int i = 0; i < controlFactorsValues.Count; i++)
                controlFactorsValuesTransfer.Add(new List<double>()); //тут будут храниться новые значения управляющих факторов
            StreamWriter output = new StreamWriter("outputTransformations.txt");

            for (int column = 0; column < transferedControlFactorsValues[0].Count; column++) {
                output.WriteLine();
                output.WriteLine("------------------------------------------------------------------");
                output.WriteLine("X{0}:", column + 1);
                var controlFactorValuesTest = new List<double>();
                var controlFactorValuesReturn = new List<double>();
                var helpTransferList = new List<double>();
                var transfersFunctionsNames = new List<string>(); //список преобразований
                controlFactorValuesTest.Clear();
                controlFactorValuesReturn.Clear();
                helpTransferList.Clear();
                for (int i = 0; i < transferedControlFactorsValues.Count; i++) {
                    controlFactorValuesTest.Add(transferedControlFactorsValues[i][column]);
                    controlFactorValuesReturn.Add(transferedControlFactorsValues[i][column]);
                    helpTransferList.Add(transferedControlFactorsValues[i][column]);
                }
                double maxCorrelation = correlationBetweenControlAndControlledFactors[column];
                double currentFunctionCorrelation = double.MinValue;
                double stopValue = 0.001;
                while (transfersFunctionsNames.Count < 2 || transfersFunctionsNames[transfersFunctionsNames.Count - 2] != "someFunc") {
                    transfersFunctionsNames.Add("someFunc");
                    //x^3
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], 3);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^3";
                    }
                    //x^2
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], 2);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^2";
                    }
                    //x^-1
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], -1);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^-1";
                    }

                    //x^-2
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], -2);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^-2";
                    }

                    //x^-3
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], -3);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^-3";
                    }
                    //ln(x)
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Log(controlFactorValuesTest[i]);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "ln(x)";
                    }

                    //x^1/2
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], 0.5);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^1/2";
                    }

                    //x^1/3
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Pow(controlFactorValuesTest[i], (double)1.0 / 3);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "x^1/2";
                    }

                    //sin(x)
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Sin(controlFactorValuesTest[i]);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "sin(x)";
                    }

                    //tg(x)
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Tan(controlFactorValuesTest[i]);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "tg(x)";
                    }

                    //arctg(x)
                    for (int i = 0; i < controlFactorValuesTest.Count; i++)
                        controlFactorValuesTest[i] = Math.Atan(controlFactorValuesTest[i]);
                    currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                        for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                            controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                            controlFactorValuesTest[i] = helpTransferList[i];
                        }
                        maxCorrelation = currentFunctionCorrelation;
                        transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "arctg(x)";
                    }
                    ////e^10x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(10 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^10x";
                    //}
                    ////e^9x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(9 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^9x";
                    //}

                    ////e^8x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(8 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^8x";
                    //}
                    ////e^7x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(7 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^7x";
                    //}
                    ////e^6x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(6 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^6x";
                    //}

                    ////e^5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^5x";
                    //}
                    ////e^4.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(4.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^4.5x";
                    //}
                    ////e^4x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(4 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^4x";
                    //}
                    ////e^3.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(3.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^3.5x";
                    //}

                    ////e^3x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(3 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^3x";
                    //}

                    ////e^2.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(2.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^2.5x";
                    //}
                    ////e^2x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(2 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^2x";
                    //}
                    ////e^1.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(1.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^1.5x";
                    //}
                    ////e^1x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(1 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^1x";
                    //}
                    ////e^0.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.5x";
                    //}
                    ////e^0.1x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.1 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.1x";
                    //}
                    ////e^0.01x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.01 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.01x";
                    //}
                    ////e^0.001x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.001 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.001x";
                    //}

                    ////e^-10x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-10 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-10x";
                    //}
                    ////e^-9x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-9 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-9x";
                    //}

                    ////e^-8x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-8 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-8x";
                    //}
                    ////e^-7x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-7 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-7x";
                    //}
                    ////e^-6x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-6 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-6x";
                    //}

                    ////e^-5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-5x";
                    //}
                    ////e^-4.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-4.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-4.5x";
                    //}
                    ////e^-4x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-4 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-4x";
                    //}
                    ////e^-3.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-3.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-3.5x";
                    //}

                    ////e^-3x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-3 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-3x";
                    //}

                    ////e^-2.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-2.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-2.5x";
                    //}
                    ////e^-2x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-2 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-2x";
                    //}
                    ////e^-1.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-1.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-1.5x";
                    //}
                    ////e^-1x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-1 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-1x";
                    //}
                    ////e^-0.5x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.5 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.5x";
                    //}
                    ////e^-0.1x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.1 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.1x";
                    //}
                    ////e^-0.01x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.01 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.01x";
                    //}
                    ////e^-0.001x
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.001 * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.001x";
                    //}



                    ////e^10x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(10 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^10x^2";
                    //}
                    ////e^9x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(9 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^9x^2";
                    //}

                    ////e^8x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(8 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^8x^2";
                    //}
                    ////e^7x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(7 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^7x^2";
                    //}
                    ////e^6x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(6 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^6x^2";
                    //}

                    ////e^5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^5x^2";
                    //}
                    ////e^4.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(4.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^4.5x^2";
                    //}
                    ////e^4x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(4 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^4x^2";
                    //}
                    ////e^3.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(3.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^3.5x^2";
                    //}

                    ////e^3x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(3 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^3x^2";
                    //}

                    ////e^2.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(2.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^2.5x^2";
                    //}
                    ////e^2x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(2 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^2x^2";
                    //}
                    ////e^1.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(1.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^1.5x^2";
                    //}
                    ////e^1x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(1 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^1x^2";
                    //}
                    ////e^0.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.5x^2";
                    //}
                    ////e^0.1x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.1 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.1x^2";
                    //}
                    ////e^0.01x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.01 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.01x^2";
                    //}
                    ////e^0.001x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(0.001 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^0.001x^2";
                    //}

                    ////e^-10x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-10 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-10x^2";
                    //}
                    ////e^-9x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-9 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-9x^2";
                    //}

                    ////e^-8x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-8 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-8x^2";
                    //}
                    ////e^-7x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-7 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-7x^2";
                    //}
                    ////e^-6x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-6 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-6x^2";
                    //}

                    ////e^-5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-5x^2";
                    //}
                    ////e^-4.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-4.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-4.5x^2";
                    //}
                    ////e^-4x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-4 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-4x^2";
                    //}
                    ////e^-3.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-3.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-3.5x^2";
                    //}

                    ////e^-3x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-3 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-3x^2";
                    //}

                    ////e^-2.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-2.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-2.5x^2";
                    //}
                    ////e^-2x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-2 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-2x^2";
                    //}
                    ////e^-1.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-1.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-1.5x^2";
                    //}
                    ////e^-1x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-1 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-1x^2";
                    //}
                    ////e^-0.5x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.5 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.5x^2";
                    //}
                    ////e^-0.1x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.1 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.1x^2";
                    //}
                    ////e^-0.01x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.01 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.01x^2";
                    //}
                    ////e^-0.001x^2
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = Math.Exp(-0.001 * controlFactorValuesTest[i] * controlFactorValuesTest[i]);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "e^-0.001x^2";
                    //}

                    ////1/(1+e^10x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(10 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^10x)";
                    //}
                    ////1/(1+e^9x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(9 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^9x)";
                    //}

                    ////1/(1+e^8x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(8 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^8x)";
                    //}

                    ////1/(1+e^7x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(7 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^7x)";
                    //}

                    ////1/(1+e^6x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(6 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^6x)";
                    //}
                    ////1/(1+e^5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^5x)";
                    //}

                    ////1/(1+e^4.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(4.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^4.5x)";
                    //}
                    ////1/(1+e^4x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(4 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^4x)";
                    //}

                    ////1/(1+e^3.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(3.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^3.5x)";
                    //}

                    ////1/(1+e^3x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(3 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^3x)";
                    //}
                    ////1/(1+e^2.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(2.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^2.5x)";
                    //}
                    ////1/(1+e^2x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(2 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^2x)";
                    //}

                    ////1/(1+e^1.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(1.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^1.5x)";
                    //}

                    ////1/(1+e^1x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(1 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^1x)";
                    //}

                    ////1/(1+e^0.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(0.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^0.5x)";
                    //}

                    ////1/(1+e^0.1x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(0.1 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^0.1x)";
                    //}

                    ////1/(1+e^0.01x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(0.01 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^0.01x)";
                    //}

                    ////1/(1+e^0.001x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(0.001 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^0.001x)";
                    //}

                    ////1/(1+e^-10x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-10 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-10x)";
                    //}
                    ////1/(1+e^-9x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-9 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-9x)";
                    //}

                    ////1/(1+e^-8x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-8 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-8x)";
                    //}

                    ////1/(1+e^-7x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-7 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-7x)";
                    //}

                    ////1/(1+e^-6x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-6 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-6x)";
                    //}
                    ////1/(1+e^-5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-5x)";
                    //}

                    ////1/(1+e^-4.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-4.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-4.5x)";
                    //}
                    ////1/(1+e^-4x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-4 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-4x)";
                    //}

                    ////1/(1+e^-3.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-3.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-3.5x)";
                    //}

                    ////1/(1+e^-3x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-3 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-3x)";
                    //}
                    ////1/(1+e^-2.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-2.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-2.5x)";
                    //}
                    ////1/(1+e^-2x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-2 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-2x)";
                    //}

                    ////1/(1+e^-1.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-1.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-1.5x)";
                    //}

                    ////1/(1+e^-1x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-1 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-1x)";
                    //}

                    ////1/(1+e^-0.5x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-0.5 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-0.5x)";
                    //}

                    ////1/(1+e^-0.1x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-0.1 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-0.1x)";
                    //}

                    ////1/(1+e^-0.01x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-0.01 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-0.01x)";
                    //}

                    ////1/(1+e^-0.001x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-0.001 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-0.001x)";
                    //}

                    ////1/(1+e^-0.001x)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = 1.0 / (1 + Math.Exp(-0.001 * controlFactorValuesTest[i]));
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "1/(1+e^-0.001x)";
                    //}


                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(10 * controlFactorValuesTest[i]) - 1) / (Math.Exp(10 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^10x-1)/(e^10x+1)";
                    //}

                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(9 * controlFactorValuesTest[i]) - 1) / (Math.Exp(9 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^9x-1)/(e^9x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(8 * controlFactorValuesTest[i]) - 1) / (Math.Exp(8 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^8x-1)/(e^8x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(7 * controlFactorValuesTest[i]) - 1) / (Math.Exp(7 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^7x-1)/(e^7x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(6 * controlFactorValuesTest[i]) - 1) / (Math.Exp(6 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^6x-1)/(e^6x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^5x-1)/(e^5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(4.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(4.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^4.5x-1)/(e^4.5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(4 * controlFactorValuesTest[i]) - 1) / (Math.Exp(4 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^4x-1)/(e^4x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(3.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(3.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^3.5x-1)/(e^3.5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(3 * controlFactorValuesTest[i]) - 1) / (Math.Exp(3 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^3x-1)/(e^3x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(2.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(2.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^2.5x-1)/(e^2.5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(2 * controlFactorValuesTest[i]) - 1) / (Math.Exp(2 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^2x-1)/(e^2x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(1.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(1.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^1.5x-1)/(e^1.5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(1 * controlFactorValuesTest[i]) - 1) / (Math.Exp(1 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^1x-1)/(e^1x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(0.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(0.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^0.5x-1)/(e^0.5x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(0.1 * controlFactorValuesTest[i]) - 1) / (Math.Exp(0.1 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^0.1x-1)/(e^0.1x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(0.01 * controlFactorValuesTest[i]) - 1) / (Math.Exp(0.01 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^0.01x-1)/(e^0.01x+1)";
                    //}
                    ////(e^ax-1)/(e^ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(0.001 * controlFactorValuesTest[i]) - 1) / (Math.Exp(0.001 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^0.001x-1)/(e^0.001x+1)";
                    //}




                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-10 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-10 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-10x-1)/(e^-10x+1)";
                    //}

                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-9 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-9 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-9x-1)/(e^-9x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-8 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-8 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-8x-1)/(e^-8x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-7 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-7 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-7x-1)/(e^-7x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-6 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-6 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-6x-1)/(e^-6x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-5x-1)/(e^-5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-4.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-4.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-4.5x-1)/(e^-4.5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-4 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-4 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-4x-1)/(e^-4x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-3.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-3.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-3.5x-1)/(e^-3.5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-3 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-3 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-3x-1)/(e^-3x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-2.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-2.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-2.5x-1)/(e^-2.5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-2 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-2 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-2x-1)/(e^-2x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-1.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-1.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-1.5x-1)/(e^-1.5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-1 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-1 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-1x-1)/(e^-1x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-0.5 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-0.5 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-0.5x-1)/(e^-0.5x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-0.1 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-0.1 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-0.1x-1)/(e^-0.1x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-0.01 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-0.01 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-0.01x-1)/(e^-0.01x+1)";
                    //}
                    ////(e^-ax-1)/(e^-ax+1)
                    //for (int i = 0; i < controlFactorValuesTest.Count; i++)
                    //    controlFactorValuesTest[i] = (Math.Exp(-0.001 * controlFactorValuesTest[i]) - 1) / (Math.Exp(-0.001 * controlFactorValuesTest[i]) + 1);
                    //currentFunctionCorrelation = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorValuesTest, controlledFactorsValues);
                    //if (Math.Abs(currentFunctionCorrelation) - Math.Abs(maxCorrelation) > stopValue) {
                    //    for (int i = 0; i < controlFactorValuesTest.Count; i++) {
                    //        controlFactorValuesReturn[i] = controlFactorValuesTest[i];
                    //        controlFactorValuesTest[i] = helpTransferList[i];
                    //    }
                    //    maxCorrelation = currentFunctionCorrelation;
                    //    transfersFunctionsNames[transfersFunctionsNames.Count - 1] = "(e^-0.001x-1)/(e^-0.001x+1)";
                    //}


                    output.WriteLine(" {0} : {1}  ", correlationBetweenControlAndControlledFactors[column], maxCorrelation);
                    controlFactorValuesReturn = HelpMethods.TransferControlFactorsToInterval2_3(controlFactorValuesReturn);
                    transfersFunctionsNames.Add("Transfer to (2-3)");
                    for (int i = 0; i < helpTransferList.Count; i++) {
                        helpTransferList[i] = controlFactorValuesReturn[i];
                        controlFactorValuesTest[i] = helpTransferList[i];  //возвращаю helpTransferList
                    }
                }
                for (int i = 0; i < transfersFunctionsNames.Count - 2; i++)
                    output.Write(transfersFunctionsNames[i] + " -> ");
                for (int i = 0; i < controlFactorValuesReturn.Count; i++)
                    controlFactorsValuesTransfer[i].Add(controlFactorValuesReturn[i]);
            }
            output.Close();

            var correlationBetweenTransferedControlFactors = HelpMethods.GetCorrelationBetweenControlFactors(controlFactorsValuesTransfer);
            var dependenceBetweenTransferedControlFactors = HelpMethods.GetDependenceBetweenControlFactorsByPirsonCorrelationCoefficient(correlationBetweenTransferedControlFactors, double.Parse(DataFromForm.tb_PirsonThersholdBetweenControlFactors));
            var correlationBetweenTransferedControlAndControlledFactors = HelpMethods.GetCorrelationBetweenControlAndControlledFactors(controlFactorsValuesTransfer, controlledFactorsValues);
            var dependenceBetweenTransferedControlAndControlledFactors = HelpMethods.GetNumericDependenceBetweenControlAndControlledFactors(correlationBetweenTransferedControlAndControlledFactors, double.Parse(DataFromForm.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors));
            var finalDependenceBetweenTransferedControlFactors = HelpMethods.GetFinalDependenceBetweenControlFactors(dependenceBetweenTransferedControlFactors, dependenceBetweenTransferedControlAndControlledFactors);
            var regressionTransferedEquationsModels = HelpMethods.CreateRegressionEquationsModels(finalDependenceBetweenTransferedControlFactors); //макеты моделей
            var bModelCoefs = new List<List<double>>(); //коэффициенты моделей
            for (int i = 0; i < regressionTransferedEquationsModels.Count; i++) { //создание моделей с предобработкой
                bModelCoefs.Add(HelpMethods.CreateEquationsWithFunc(controlFactorsValuesTransfer, controlledFactorsValues, regressionTransferedEquationsModels[i]));
            }

            //regressionTransferEquationMidels - модели с функциональной предобработкой 

            HelpMethods.outputWithFunc.Close();
            HelpMethods.outputWithoutFunc.Close();

            //----------------------------------------------------------------------------------------------------
            var bestModel = new List<double>(); //лучшая модель для жок
            var bestModelCoefs = new List<double>(); //коэффициенты лучшей модели
            StreamWriter write = new StreamWriter("bestModel.txt"); //сюда запишем лучшую полученную модель
            if (DataFromForm.tb1 == "0") {
                for (int i = 0; i < regressionTransferedEquationsModels[0].Count; i++)
                    bestModel.Add(regressionTransferedEquationsModels[0][i]);
                for (int i = 0; i < bModelCoefs[0].Count; i++)
                    bestModelCoefs.Add(bModelCoefs[0][i]);
            }

            if (DataFromForm.tb1 == "1") {
                string[] valuesStr = DataFromForm.tb2.Split(';');
                var valuesExperts = new List<double>(); // оценки важности экспертов
                for (int i = 0; i < valuesStr.Length; i++)
                    valuesExperts.Add(double.Parse(valuesStr[i]));
                double max = 0;
                int modelIndex = 0;
                for (int i = 0; i < regressionTransferedEquationsModels.Count; i++) {
                    double sum = 0;
                    for (int j = 0; j < regressionTransferedEquationsModels[i].Count; j++)
                        sum += valuesExperts[(int)regressionTransferedEquationsModels[i][j]];
                    if (sum > max) {
                        max = sum;
                        modelIndex = i;
                    }
                }
                for (int i = 0; i < regressionTransferedEquationsModels[modelIndex].Count; i++)
                    bestModel.Add(regressionTransferedEquationsModels[modelIndex][i]);
                for (int i = 0; i < bModelCoefs[modelIndex].Count; i++)
                    bestModelCoefs.Add(bModelCoefs[modelIndex][i]);

            }

            if (DataFromForm.tb1 == "2") {
                string[] valuesStr = DataFromForm.tb2.Split(';');
                var valuesExperts = new List<double>(); // оценки важности экспертов
                for (int i = 0; i < valuesStr.Length; i++)
                    valuesExperts.Add(double.Parse(valuesStr[i]));
                double max = 0;
                int modelIndex = 0;
                for (int i = 0; i < regressionTransferedEquationsModels.Count; i++) {
                    double sum = 0;
                    for (int j = 0; j < regressionTransferedEquationsModels[i].Count; j++)
                        sum += valuesExperts[(int)regressionTransferedEquationsModels[i][j]] / regressionTransferedEquationsModels[i].Count;
                    if (sum > max) {
                        max = sum;
                        modelIndex = i;
                    }
                }
                for (int i = 0; i < regressionTransferedEquationsModels[modelIndex].Count; i++)
                    bestModel.Add(regressionTransferedEquationsModels[modelIndex][i]);
                for (int i = 0; i < bModelCoefs[modelIndex].Count; i++)
                    bestModelCoefs.Add(bModelCoefs[modelIndex][i]);

            }
            write.WriteLine("Лучшая регрессионная модель для дальнейшего анализа:");
            write.Write(bestModelCoefs[0]);
            for (int i = 0; i < bestModel.Count; i++)
                write.Write(" + ({0})X{1}", bestModelCoefs[i + 1], bestModel[i] + 1);
            write.Close();

            //выбор лучшей модели реализован. Модель хранится в List bestModel, ее коэффициенты в List bestModelCoef
            //записана в файл bestModel.txt
            var groupsCount = 10; //будет 10 групп для приведения к нормальному распеделению
            var groupElementsCount = transferedControlFactorsValues.Count / groupsCount;
            var bestModelControlFactorsValues = new List<List<double>>();
            for (int i = 0; i < transferedControlFactorsValues.Count; i++) {
                bestModelControlFactorsValues.Add(new List<double>());
                for (int j = 0; j < bestModel.Count; j++)
                    bestModelControlFactorsValues[i].Add(transferedControlFactorsValues[i][(int)bestModel[j]]);
            }
            var maxMultiCoefs = new List<double>();//максимальный коэф. увеличения значений
            var minMultiCoefs = new List<double>();//коэф. уменьшения значений
            var averageValuesForControlFactors = new List<List<double>>(); //для каждого упр. фактора хранит средние групповые значения

            for (int i = 0; i < groupsCount; i++)
                averageValuesForControlFactors.Add(new List<double>());

            //определение размаха для каждого управляющего фактора с выводом в файл
            StreamWriter writeRazmah = new StreamWriter("Razmah.txt");
            int groupNumber = 0;
            double summ = 0;
            for (int i = 0; i < bestModelControlFactorsValues[0].Count; i++) {
                summ = 0;
                groupNumber = 0;
                for (int j = 0; j <= bestModelControlFactorsValues.Count; j++) {
                    if (j != 0 && j % groupElementsCount == 0) {
                        averageValuesForControlFactors[groupNumber].Add(summ / groupElementsCount);
                        summ = 0;
                        groupNumber++;
                    }
                    if (j < bestModelControlFactorsValues.Count)
                        summ += bestModelControlFactorsValues[j][i];
                }
            }
            var matOzhidanieForFactor = new List<double>();
            var standartOtklonForFactor = new List<double>();
            for (int i = 0; i < averageValuesForControlFactors[0].Count; i++) {
                double summm = 0;
                for (int j = 0; j < averageValuesForControlFactors.Count; j++)
                    summm += averageValuesForControlFactors[j][i];
                matOzhidanieForFactor.Add(summm / groupsCount);
            }
            for (int i = 0; i < averageValuesForControlFactors[0].Count; i++) {
                double summm = 0;
                for (int j = 0; j < averageValuesForControlFactors.Count; j++)
                    summm += Math.Pow(averageValuesForControlFactors[j][i] - matOzhidanieForFactor[i], 2);
                standartOtklonForFactor.Add(Math.Sqrt(summm / groupsCount));
            }
            writeRazmah.WriteLine("Рекомендуемые границы изменения значений: ");
            for (int i = 0; i < matOzhidanieForFactor.Count; i++) {
                maxMultiCoefs.Add((matOzhidanieForFactor[i] + 3 * standartOtklonForFactor[i]) / matOzhidanieForFactor[i]);
                minMultiCoefs.Add((matOzhidanieForFactor[i] - 3 * standartOtklonForFactor[i]) / matOzhidanieForFactor[i]);
                writeRazmah.WriteLine("Фактор X{0}: рекомендуемые коэффициенты размаха значений: {1} - {2}", bestModel[i] + 1, minMultiCoefs[i], maxMultiCoefs[i]);
            }
            writeRazmah.Close();  //определение размаха сделано.
            var averageControlledFactorValue = controlledFactorsValues.Average();

            // averagecontrolledFactorValue - среднее значение Y до ЖОК
            // matOzhidanieForFactor - среднее значение Xi до ЖОК
            // bestModelControlFactorValues - управляющие факторы модели
            //выбор лучшей модели реализован. Модель хранится в List bestModel, ее коэффициенты в List bestModelCoef
            var controlFactorsModelsCoefs = new List<List<double>>(); //коэффициенты принудительных моделей
            var controlFactorsModels = new List<List<double>>(); //принудительные модели
            var modelIterator = 0;
            while (modelIterator < bestModelControlFactorsValues[0].Count) {
                controlFactorsModels.Add(new List<double>());
                for (int i = 0; i < bestModelControlFactorsValues[0].Count; i++)
                    if (i != modelIterator)
                        controlFactorsModels[modelIterator].Add(i);
                modelIterator++;
            }
            for (int i = 0; i < controlFactorsModels.Count; i++) {
                var controlledFactorValuesForModeling = new List<double>();
                for (int p = 0; p < controlFactorsValues.Count; p++) {
                    controlledFactorValuesForModeling.Add(bestModelControlFactorsValues[p][i]);
                }
                controlFactorsModelsCoefs.Add(HelpMethods.CreateEquationsForControlFactors(bestModelControlFactorsValues, controlledFactorValuesForModeling, controlFactorsModels[i]));
                controlledFactorValuesForModeling.Clear();
            }
            //вывод взаимовлияний
            StreamWriter wr = new StreamWriter("РегрессионныеУравненияВзаимовлияний.txt");
            wr.WriteLine("Модели взаимовлияний: ");
            int modelNumber = 0;
            for (int i = 0; i < controlFactorsModels.Count; i++) {
                wr.Write("X{0} = {1} ", bestModel[modelNumber] + 1, controlFactorsModelsCoefs[i][0]);
                int k = 0;
                for (int j = 1; j < controlFactorsModelsCoefs.Count; j++) {
                    if (k == modelNumber)
                        k++;
                    wr.Write("+ ({0})X{1} ", controlFactorsModelsCoefs[i][j], bestModel[k] + 1);
                    k++;
                }
                modelNumber++;
                wr.WriteLine();
            }
            wr.Close();
            //конец вывода взаимовлияний

            //Начало рассчета метода ЖОК
            // averagecontrolledFactorValue - среднее значение Y до ЖОК
            // matOzhidanieForFactor - среднее значение Xi до ЖОК
            // bestModelControlFactorValues - управляющие факторы модели
            //выбор лучшей модели реализован. Модель хранится в List bestModel, ее коэффициенты в List bestModelCoef
            //controlFactorsModelsCoefs
            if (DataFromForm.tb3 == "-1")
                Application.Exit();
            else {
                var changedControlFactorNumber = int.Parse(DataFromForm.tb3);
                var changeCoef = double.Parse(DataFromForm.tb4);
                var lastIterationValues = new List<double>();
                var currentIterationValues = new List<double>();
                for (int i = 0; i < matOzhidanieForFactor.Count; i++) {
                    lastIterationValues.Add(matOzhidanieForFactor[i]);
                    currentIterationValues.Add(matOzhidanieForFactor[i]);
                }
                currentIterationValues[changedControlFactorNumber] *= changeCoef;
                var count = lastIterationValues.Count;
                for (int q = 0; q < 4; q++) {//   while (!GetError(lastIterationValues, currentIterationValues)) {
                    lastIterationValues.Clear();
                    for (int i = 0; i < count; i++)
                        lastIterationValues.Add(currentIterationValues[i]);
                    for (int i = 0; i < count; i++)
                        currentIterationValues[i] = 0;
                    for (int i = 0; i < count; i++) {
                        if (i == changedControlFactorNumber)
                            currentIterationValues[i] = lastIterationValues[i];
                        else {
                            double sum = 0;
                            sum += controlFactorsModelsCoefs[i][0];
                            for (int p = 1; p < controlFactorsModelsCoefs[i].Count; p++) {
                                sum += controlFactorsModelsCoefs[i][p] * lastIterationValues[p - 1];
                            }
                            currentIterationValues[i] = sum;
                        }
                    }
                }
                //currentIterationValues
                //matOzhidanieForFactor
                //averagecontrolledFactorValue
                //bestModel
                StreamWriter zhok = new StreamWriter("ЖОК.txt");
                zhok.WriteLine("Коэффициент изменения управляющих факторов: ");
                for (int i = 0; i < matOzhidanieForFactor.Count; i++)
                    zhok.WriteLine("X{0}: {1}", bestModel[i] + 1, currentIterationValues[i] / matOzhidanieForFactor[i]);
                zhok.WriteLine("Коэффициент изменения управляемого фактора: ");
                double newYvalue = bestModelCoefs[0];
                for (int i = 1; i < bestModelCoefs.Count; i++)
                    newYvalue += bestModelCoefs[i] * currentIterationValues[i-1];
                zhok.WriteLine("Y:{0}", newYvalue / averageControlledFactorValue);
                zhok.Close();
                Application.Exit();

            }
        }
    }
}