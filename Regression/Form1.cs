﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using Regression;

namespace Regression {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }
        static object[,] dataFromExcel; //данные из экселя (глобальная переменная)
        private void MainForm_Load(object sender, EventArgs e) {
        }
        private void btnOpenShowDialog_Click(object sender, EventArgs e) {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файл Excel|*.XLSX;*.XLS";
            if (openDialog.ShowDialog() == DialogResult.OK) {
                Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook excelWorkBook = excelApp.Workbooks.Open(openDialog.FileName, 0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelWorkBook.Sheets[1];
                var excelLastRow = excelWorkSheet.Cells.Find("*", System.Reflection.Missing.Value,
                               System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                               Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious,
                               false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Row;
                var excelLastColumn = excelWorkSheet.Cells.Find("*", System.Reflection.Missing.Value,
                                System.Reflection.Missing.Value, System.Reflection.Missing.Value,
                                Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, Microsoft.Office.Interop.Excel.XlSearchDirection.xlPrevious,
                                false, System.Reflection.Missing.Value, System.Reflection.Missing.Value).Column;
                var excelRange = "A1:" + HelpMethods.GetColumnName(excelLastColumn) + excelLastRow;
                dataFromExcel = (object[,])excelWorkSheet.Range[excelRange].Value;
                excelWorkBook.Close(false, Type.Missing, Type.Missing);
                excelApp.Quit();
                GC.Collect();
                string message = "Данные из файла загружены в программу!";
                string caption = "Загрузка данных";
                MessageBoxButtons button = MessageBoxButtons.OK;
                DialogResult result;
                result = MessageBox.Show(message, caption, button);
                if (result == DialogResult.OK) {
                    btnOpenShowDialog.Enabled = false;
                    btn_ConfirmEnteredData.Enabled = true;
                }
            }
        }

        private void tb_ThresholdCorrelationBetweenControlFactors_KeyPress(object sender, KeyPressEventArgs e) {
            if (tb_ThresholdCorrelationBetweenControlFactors.TextLength <= 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) || e.KeyChar == (char)Keys.Back)
                    e.Handled = true;
            if (tb_ThresholdCorrelationBetweenControlFactors.TextLength > 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != (char)Keys.Back)
                    e.Handled = true;
        }
        private void tb_ThresholdCorrelationBetweenControlFactors_TextChanged(object sender, EventArgs e) {
            if (tb_ThresholdCorrelationBetweenControlFactors.Text[0] != '0' || tb_ThresholdCorrelationBetweenControlFactors.Text[1] != ',')
                tb_ThresholdCorrelationBetweenControlFactors.Text = "0,";
            DataFromForm.tb_PirsonThersholdBetweenControlFactors = tb_ThresholdCorrelationBetweenControlFactors.Text;
        }

        private void btn_Exit_Click(object sender, EventArgs e) {
            GC.Collect();
            System.Windows.Forms.Application.Exit();
        }

        private void btn_ConfirmEnteredData_Click(object sender, EventArgs e) {
            if (DataFromForm.tb_PirsonThersholdBetweenControlFactors != null &&
                (checkBoxNumericValue.Checked || checkBoxStatisticalHypothesis.Checked) &&
                (DataFromForm.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors != null || DataFromForm.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors != null)) {
                btn_StartCalculations.Enabled = true;
                btn_ConfirmEnteredData.Enabled = false;
            }
            else {
                string message = "Вы ввели не все необходимые данные для начала рассчета!";
                string caption = "Начало рассчетов";
                MessageBoxButtons button = MessageBoxButtons.OK;
                DialogResult result;
                MessageBox.Show(message, caption, button);
            }
            DataFromForm.tb1 = textBox1.Text;
            DataFromForm.tb2 = textBox2.Text;
            DataFromForm.tb3 = textBox3.Text;
            DataFromForm.tb4 = textBox4.Text;


        }

        private void tb_NumericTresholdCorrelationBetweenControlAndControlledFactors_TextChanged(object sender, EventArgs e) {
            if (tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text[0] != '0' || tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text[1] != ',')
                tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
            DataFromForm.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors = tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text;
        }

        private void tb_NumericTresholdCorrelationBetweenControlAndControlledFactors_KeyPress(object sender, KeyPressEventArgs e) {
            if (tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.TextLength <= 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) || e.KeyChar == (char)Keys.Back)
                    e.Handled = true;
            if (tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.TextLength > 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != (char)Keys.Back)
                    e.Handled = true;
        }

        private void tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors_KeyPress(object sender, KeyPressEventArgs e) {
            if (tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.TextLength <= 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) || e.KeyChar == (char)Keys.Back)
                    e.Handled = true;
            if (tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.TextLength > 2)
                if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != (char)Keys.Back)
                    e.Handled = true;
        }

        private void tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors_TextChanged(object sender, EventArgs e) {
            if (tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text[0] != '0' || tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text[1] != ',')
                tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
            DataFromForm.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors = tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text;
        }

        private void checkBoxNumericValue_CheckedChanged(object sender, EventArgs e) {
            if (checkBoxNumericValue.Checked) {
                DataFromForm.checkBoxStatisticalHypothesis = false;
                DataFromForm.checkBoxNumericValue = true;
                checkBoxStatisticalHypothesis.Enabled = false;
                lb_StatisticalValue.Enabled = false;
                tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Enabled = false;
                tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
                DataFromForm.tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors = null;
            }
            else {
                DataFromForm.checkBoxStatisticalHypothesis = true;
                DataFromForm.checkBoxNumericValue = false;
                checkBoxStatisticalHypothesis.Enabled = true;
                lb_StatisticalValue.Enabled = true;
                tb_StatisticalTresholdCorrelationBetweenControlAndControlledFactors.Enabled = true;
            }
        }

        private void checkBoxStatisticalHypothesis_CheckedChanged(object sender, EventArgs e) {
            if (checkBoxStatisticalHypothesis.Checked) {
                DataFromForm.checkBoxStatisticalHypothesis = true;
                DataFromForm.checkBoxNumericValue = false;
                checkBoxNumericValue.Enabled = false;
                lb_NumericValue.Enabled = false;
                tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Enabled = false;
                tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Text = "0,";
                DataFromForm.tb_NumericTresholdCorrelationBetweenControlAndControlledFactors = null;
            }
            else {
                DataFromForm.checkBoxStatisticalHypothesis = false;
                DataFromForm.checkBoxNumericValue = true;
                checkBoxNumericValue.Enabled = true;
                lb_NumericValue.Enabled = true;
                tb_NumericTresholdCorrelationBetweenControlAndControlledFactors.Enabled = true;
            }
        }

        private void btn_StartCalculations_Click(object sender, EventArgs e) {
            Regression.RegressionEquations.GetDataFromExcel(dataFromExcel);
        }

        private void label2_Click(object sender, EventArgs e) {

        }

        private void label1_Click(object sender, EventArgs e) {

        }
    }
}